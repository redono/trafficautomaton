package fbalm.checkpoints.traffic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


/**
 * Class that holds the array of car cells and draws the relevant pictures.
 * @author Floris Balm
 *
 */
public class AutomatonPanel extends JPanel{
    
    //Animation colours, default red and black
    private static Color[] colours = new Color[2];
    
    private javax.swing.Timer timer;
    
    ArrayList<double[]> data;
    private int numberOfCells;    
    private double currentDensity;
    
    static {
        colours[0] = new Color(0,0,0);
        colours[1] = new Color(255,0,0);
    }
    
    private CellArray cellGrid;
    
    /**
     * 
     * @param numberOfCells Total number of cells for the grid
     * @param density Density of cars in the grid
     */
    public AutomatonPanel(int numberOfCells, double density)
    {
        data = new ArrayList<>();
        
        this.currentDensity = density;
        this.numberOfCells = numberOfCells;
        
        setMinimumSize(new Dimension(25*numberOfCells,25));
        setPreferredSize(new Dimension(25*numberOfCells,25));
        
        setGrid(numberOfCells,density);
        
        repaint();
        
        timer = new Timer(200, (e)->
                    {
                        cellGrid.step();
                        repaint();
                    });
        timer.setInitialDelay(1000);
    }
    @Override
    /**
     * Overrides paint for the panel
     */
    
    public void paint(Graphics g)
    {
        super.paint(g);
        
        for(int i = 0; i<cellGrid.size(); ++i)
        {
            //Empty cell
            if (!cellGrid.isOccupied(i))
            {
                //Backgroundcolor
                g.setColor(colours[0]);
            } else { //Non-empty cell
                //Carcolor
                g.setColor(colours[1]);
            }
            //Spread cells over entire width
            int rectangleWidth = (int)Math.round(((double)this.getWidth())/((double)cellGrid.size()));
            g.fillRect(i*rectangleWidth, 0, rectangleWidth , this.getHeight());
        }
        
    }
    
    /**
     * Randomly generate a grid from a number of cells and a density
     * @param numberOfCells Number of cells (cars+empty space) on the road
     * @param density Double between 0.0 and 1.0 for the ratio cars:total road space
     */
    public void setGrid(int numberOfCells, double density)
    {
        this.currentDensity = density;
        this.numberOfCells = numberOfCells;
        this.cellGrid = new CellArray(numberOfCells, density);
    }
    
    /**
     * Set the grid of cars to a certain predefined grid.
     * @param cells Grid that the cars will be placed equal to
     */
    public void setGrid(boolean[] cells)
    {
        //New array, discard the old
        cellGrid = new CellArray();
        
        for (int i = 0; i<cells.length; i++)
        {
            cellGrid.add(new CarCell(cells[i]));
        }
        
        setMinimumSize(new Dimension(cells.length*25,25));
        setPreferredSize(new Dimension(cells.length*25,25));
        
        repaint();
    }
    
    /**
     * Run the test for a series of increasing densities. The result of average velocity vs time will
     * be displayed on a separate frame.
     * @param densityStep The increments for density to take in the graph
     */
    public void runTest(double densityStep, int testNumberOfCells)
    {
        //Start with the density of densityStep. 0 density is impossible
        double density = densityStep;
        
        //Copy current density to restore after test
        double initialDensity = currentDensity;
        int initialNumberOfCells = numberOfCells;
        //The maximum density is 1.0
        int numberOfSteps = (int) (1.0/densityStep);
        
        for(int i = 0; i < numberOfSteps; ++i)
        {
            //Limit the number of steps to 100, should be enough for a good estimate
            for(int stepCount = 0; stepCount<250; stepCount++)
            {
                cellGrid.step();
            }
            
            //Store the data
            data.add(new double[] {density, cellGrid.getAverageVelocity()});
            
            density += densityStep;
            setGrid(testNumberOfCells,density);
        }
        
        //Holds the data for further processing
        XYSeries series = new XYSeries("Average Velocity vs Density");
        
        //Add all points (x,y) where x is density, y is velocity to the series
        data.forEach((xyPair) -> series.add(xyPair[0],xyPair[1]));
        
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        
        //Create a chart from the XY data
        JFreeChart chart = ChartFactory.createXYLineChart("Average Velocity vs Density",
                                            "Density of cars", "Average velocity", 
                                            dataset,
                                            PlotOrientation.VERTICAL,
                                            false, //No legend
                                            true,  //Axis labels
                                            false);
       
       //Display the chart
       ChartFrame frame = new ChartFrame("Average velocity vs density of cars",chart);
       frame.setVisible(true);
       frame.setSize(600,400);
       frame.addWindowListener(new WindowAdapter(){
           @Override
           public void windowClosing(WindowEvent ev){
               
               //Chart might want to be saved
               int response = JOptionPane.showConfirmDialog(null, "Do you wish to save the chart?", "Save", JOptionPane.YES_NO_OPTION);
               
               if(response == JOptionPane.YES_OPTION)
               {
                   //Filter on JPEG/JPG only
                   FileFilter filter = new FileNameExtensionFilter(".jpg",new String[]{"jpg", "jpeg"});
                   JFileChooser fc = new JFileChooser();
                   fc.setFileFilter(filter);
                   fc.addChoosableFileFilter(filter);
                   
                   int saveResponse = fc.showSaveDialog(null);
                   //If a file has been selected
                   if(saveResponse == JFileChooser.APPROVE_OPTION)
                   {
                       File selectedFile = fc.getSelectedFile();
                       String filePath = selectedFile.getAbsolutePath();
                       
                       //If it doesn't match with .jpeg/.jpg at the end
                       if(!filePath.matches(".*\\.(jpeg|jpg)"))
                       {
                           //If it currently has an extension
                           if(filePath.contains(".") && (filePath.lastIndexOf(File.separator)<filePath.lastIndexOf(".")))
                           {
                               //Cut the file path off before the extension point
                               filePath = filePath.substring(0,filePath.lastIndexOf("."));
                           }
                           //Add .jpg as an extension
                           filePath += ".jpg";
                           //Change the file to the new path
                           selectedFile = new File(filePath);
                       }
                       try 
                       {
                           //Write the chart out as a jpeg
                           ChartUtilities.saveChartAsJPEG(selectedFile, chart, 600, 400);
                       } catch (IOException ex) {
                           JOptionPane.showMessageDialog(null, "Could not save chart", "Error", JOptionPane.ERROR_MESSAGE);
                       }
                       
                   }
               }
           }
       });
       
       frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
       
       //Set the grid back the stats it had before the test
       setGrid(initialNumberOfCells,initialDensity);
       repaint();
    }
    
    /**
     * Change drawing colors
     * @param newColours New colors to draw with
     */
    public static void setColor(Color[] newColours) throws Exception
    {
        if (newColours.length == 2 && newColours[0] != null && newColours[1]!=null)
        {
            colours = newColours; 
        } else {
            throw(new Exception());
        }
    }
    
    public void startTimer()
    {
        timer.start();
    }
    public void stopTimer()
    {
        timer.stop();
    }
    
    
}
