package fbalm.checkpoints.traffic;

/**
 * Cell in the grid of cells for the simulation. Holds the current and next states for this cell
 * @author Floris Balm
 *
 */
public class CarCell {
    
    private boolean currentState;
    private boolean nextState;
    
    public CarCell(boolean state)
    {
        currentState = state;
        nextState = false;
    }
    
    public void setState(boolean state)
    {
        nextState = state;
    }
    
    public boolean getState()
    {
        return currentState;
    }
    
    /**
     * After a step, this method finalizes it and makes the next state of the cell the current state
     */
    public void finalize()
    {
        currentState = nextState;
        nextState = false;
    }
    @Override
    public String toString()
    {
        return (currentState?("1"):("0"));
    }
}
