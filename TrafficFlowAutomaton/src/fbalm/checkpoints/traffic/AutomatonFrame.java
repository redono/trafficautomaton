package fbalm.checkpoints.traffic;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class AutomatonFrame extends JFrame {
    
    private AutomatonPanel panel;
    private ButtonPanel buttonPanel;
    
    public AutomatonFrame()
    {
        super("1D traffic flow automaton");
        
        this.setLayout(new BorderLayout());

        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Enter the number of cells:");
        int numberOfCells = keyboard.nextInt();
        System.out.println("Enter a density of cars:");
        double density = keyboard.nextDouble();
        
        //Only integers in the range [0,1) are valid
        if(density >= 1.0 || density <= 0.0){
            System.err.println("Invalid density entered.");
            System.exit(-1);
        }
        keyboard.close();
        
        panel = new AutomatonPanel(numberOfCells, density);
        this.add(panel, BorderLayout.CENTER);
        
        buttonPanel = new ButtonPanel();
        this.add(buttonPanel, BorderLayout.PAGE_END);
        
        
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        pack();
        
        
    }
    
    public static void main(String[] args) {
        //Start program by creating the frame
        EventQueue.invokeLater(() -> new AutomatonFrame());
    }
    
    //Inner class that holds the buttons for the 
    private class ButtonPanel extends JPanel
    {
        ButtonPanel()
        {
            super();
            setBackground(Color.WHITE);
            
            
            
            
            //Button to start the animation
            JButton startButton = new JButton("Start");
            startButton.addActionListener((e)->panel.startTimer());
            startButton.setVisible(true);
            add(startButton);
            
            
            //Button to stop the animation
            JButton stopButton = new JButton("Stop");
            stopButton.addActionListener((e)->panel.stopTimer());
            stopButton.setVisible(true);
            add(stopButton);
            
            
            JButton testButton = new JButton("Run test");
            testButton.addActionListener(
                    (e)->
                        {
                            //The density be plotted with increments of denistyStep to give the final result
                            String densityStep = JOptionPane.showInputDialog(null, 
                                                        "Enter a value for the step size of the density");
                            String gridLength = JOptionPane.showInputDialog(null,
                                                        "Enter the number of cells in the road");
                            //User entered a value
                            if(densityStep != null && gridLength != null)
                            {
                                try {
                                    double densityResponse = Double.parseDouble(densityStep);
                                    int cellNumberResponse = Integer.parseInt(gridLength);
                                    
                                    //Response close enough to 1 could result in divide by 0 error
                                    if (densityResponse > 0.001 && densityResponse < 1)
                                    {
                                        panel.runTest(densityResponse, Math.max(cellNumberResponse, 1));
                                    } else 
                                    {
                                        JOptionPane.showMessageDialog(null, 
                                                "The value was out of bounds", 
                                                "Out of bounds error", 
                                                JOptionPane.ERROR_MESSAGE);
                                    }
                                } catch (NumberFormatException ex) {
                                    JOptionPane.showMessageDialog(null, 
                                            "Number entered is invalid, please try again", 
                                            "Error in number input", 
                                            JOptionPane.ERROR_MESSAGE);
                                }
                            }
                        });
            testButton.setVisible(true);
            add(testButton);
            
            //Button to set the grid to new values
            JButton randomButton = new JButton("Generate");
            randomButton.addActionListener(
                    (evt)->
                        {
                            panel.stopTimer();
                            String numberOfCells = JOptionPane.showInputDialog(null,
                                                        "Enter the total number of cells in the simulation");
                            String inputDensity = JOptionPane.showInputDialog(null, 
                                                        "Enter a value for the density of cars");
                            //If something has been entered for both
                            if ((numberOfCells!=null) && (inputDensity != null))
                            {
                                try {
                                    
                                    int cellNumberResponse = Integer.parseInt(numberOfCells);
                                    double densityResponse = Double.parseDouble(inputDensity);
                                    //Very low density could result in divide by 0 error
                                    if ((densityResponse>0.0001 && densityResponse<1.0) && cellNumberResponse > 0)
                                    {
                                            panel.setGrid(cellNumberResponse,densityResponse);
                                            panel.repaint();
                                    } else 
                                    {
                                        JOptionPane.showMessageDialog(null, 
                                                "The values are out of bounds, please try again", 
                                                "Out of bounds error", 
                                                JOptionPane.ERROR_MESSAGE);
                                    }
                                    
                                } catch (NumberFormatException ex) //Input not correct
                                {
                                    JOptionPane.showMessageDialog(null, 
                                            "Numbers entered are invalid, please try again", 
                                            "Error", 
                                            JOptionPane.ERROR_MESSAGE);
                                }
                            }
            });
            randomButton.setVisible(true);
            add(randomButton);
            
            //Load a configuration from txt/csv file
            JButton loadButton = new JButton("Load");
            loadButton.addActionListener(
                    
                    (evt)-> { 
                        
                        panel.stopTimer();
                        
                        try{
                            //Text should be comma-separated
                            FileFilter filter = new FileNameExtensionFilter(".csv and .txt", new String[] {"csv","txt"});
                            JFileChooser fc = new JFileChooser();
                            //Limit file types to be chosen
                            fc.setFileFilter(filter);
                            fc.addChoosableFileFilter(filter);
                            
                            //A file has been chosen in the file chooser.
                            if (fc.showSaveDialog(null)== JFileChooser.APPROVE_OPTION)
                            {
                                BufferedReader reader = new BufferedReader(new FileReader(fc.getSelectedFile()));
                                String line = reader.readLine();
                                reader.close();
                                
                                //Separate out the values
                                String[] setup = line.split(",");
                                boolean[] startingGrid = new boolean[setup.length];
                                
                                for(int i = 0; i<setup.length; ++i)
                                {
                                    if (setup[i].equals("1"))
                                    {
                                        startingGrid[i] = true;
                                    } else {
                                        startingGrid[i] = false;
                                    }
                                }
                                
                                panel.setGrid(startingGrid);
                                
                                //Resize the frame
                                pack();
                            }
                            
                            
                        } catch (IOException ex) //Filechooser exception
                        {
                            //Error message
                            JOptionPane.showMessageDialog(null, 
                                    "There was an error in reading the file or the file was not found. Please try again.",
                                    "Error reading file", 
                                    JOptionPane.ERROR_MESSAGE);
                        }
            });
            add(loadButton);
            
            //Change the colors of the background and cars in the animation
            JButton colorChooseButton = new JButton("Choose colors");
            colorChooseButton.addActionListener(
                    (evt)->{
                        Color[] newColours = new Color[2];
                        //Black and red are standard colors
                        newColours[0] = JColorChooser.showDialog(null,  "Choose a background color",  Color.BLACK);
                        newColours[1] = JColorChooser.showDialog(null, "Pick a car color", Color.RED);
                        try {
                            AutomatonPanel.setColor(newColours);
                        } catch (Exception ex)
                        {
                            JOptionPane.showMessageDialog(null, 
                                    "There was an error in choosing the new colors. Please try again",
                                    "Error choosing colors", 
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        
                        panel.repaint();
                    });
            
            add(colorChooseButton);
        }
    }
}
