package fbalm.checkpoints.traffic;

import java.util.ArrayList;
import java.util.Collections;

public class CellArray extends ArrayList<CarCell> {
    
    private int totalDistanceMoved;
    private int totalTime;
    
    /**
     * Create a CellArray randomly filled to a certain density
     * @param numberOfCells size of the grid
     * @param density part of the cells that should be filled
     */
    public CellArray(int numberOfCells, double density)
    {
        super();
        
        int numberOfCars = (int)(density * (double)numberOfCells);
        
        //For all cells
        for(int i = 0; i < numberOfCells; i++)
        {
            //If less than numberOfCars have been added already
            add(new CarCell((i < numberOfCars)));
        }
        
        //Randomize which cells are filled
        Collections.shuffle(this);
    }
    
    public CellArray()
    {
        super();
    }
    
    /**
     * Get the average velocity over all timesteps for this 
     * @return
     */
    public double getAverageVelocity()
    {
        return (double)totalDistanceMoved/(double)totalTime;
    }
    
    public boolean isOccupied(int position)
    {
        return (get(position).getState());
    }
    
    public void setOccupied(int position)
    {
        get(position).setState(true);
    }
    
    public void setEmpty(int position)
    {
        get(position).setState(false);
    }
    /**
     * Perform a time step through the array, using the update rules to 
     */
    public void step()
    {
        int moveCounter = 0; //Number of units moved for all cars
        int timeCounter = 0; //Units of time passed for all cars
        
        for(int i = 0; i<size(); ++i)
        {
            if(isOccupied(i)) //This cell is occupied
            {
                //Position i+1, or 0 if i+1 is after the last element, i.e. equal to size()
                int nextPos = (i+1+size())%size();
                
                if(isOccupied(nextPos)){
                    setOccupied(i); //Next cell occupied, this doesn't move
                } else {
                    setEmpty(i); //Next cell free, move forward, empty this cell
                    
                    //A car has moved out of this cell. Now one car has moved a unit
                    moveCounter++;
                }
                
                //Regardless of whether the car in this cell has moved, it had to wait a step/second
                timeCounter++;
            } else {  //Cell is free
                
                //Position i-1, or size()-1 if i-1 is one before the first element
                int prevPos = (((i-1)+size())%size());
                
                if(isOccupied(prevPos)){
                    setOccupied(i); //Previous cell occupied, car there moves forward, fill this cell
                } else {
                    setEmpty(i); //Previous cell empty, cell stays empty
                }
            }
        }
        
        totalDistanceMoved += moveCounter;
        totalTime += timeCounter;
        //Make next position current position.
        this.forEach((c)->c.finalize());
    }
    

}
